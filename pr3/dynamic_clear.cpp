#include "clear.h"
#include <iostream>
using namespace std;

bool clear(void) {
    try {
        for (int i = 0; i < 100; i++) {
            cout << endl; 
        }
        return true;
    } catch(...) {
        return false;
    }
}