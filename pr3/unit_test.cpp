#define CATCH_CONFIG_MAIN
#include <iostream>
#include <catch.hpp>
#include "clear.cpp"
using namespace std;


TEST_CASE("check clear", "[check_clear]") { 
	REQUIRE(clear() == true);
}