    global testf
    section .text
testf:
        mov r13, rdi
        mov r14, rsi
        mov r15, rdx

        xor rdx, rdx
        mov rax, r13
        mov rbx, 4
        idiv rbx
        mov r8, rax

        xor rax, rax
        xor rdx, rdx
        xor rbx, rbx
        mov rax, r14
        mov rbx, r15
        idiv rbx
        mov r9, rdx

        add r8, r9

        shl r8, 2
        mov rax, r8 

        ret