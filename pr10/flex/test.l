%{
#include <math.h>
int error = 0; 
%}

DIGIT    [0-9]

%%
if|else|then|begin|>|<|<=|>=|==|<>|writeln|write|end|; {}
DIGIT|[a-z]|[A-Z] { error++; }
. {}
%%

int main( int argc, char **argv )
{
    yyin = fopen("govno.pas", "r" );
    yylex();
    if (error == 0) {
        printf("Сканирование прошло без ошибок\n");
    } else {
        printf("Во время сканирования произошла ошибка\n");
    }
    return 0;
}