#include <iostream>
#include "Work.h"
using namespace std;

Work work;

void start() {
    auto multipleAndBiggerValue = work.multipleAndBigger(); 
    if (multipleAndBiggerValue != -1)
        cout << "Наибольшее число кратное 10 " << work.multipleAndBigger() << endl;
    else 
        cout << "Наибольшее число кратное 10 отсутствует" << endl;
    cout << "Вектор после удаления всех чисел, в которых разряд десятков и единиц совпадает:" << endl;
    work.removeRankOfTensAndUnitsCoincides();
    for (int element : work.getValues()) 
        cout << element << " "; 
    cout << endl;
    cout << "Вектор после добавления после каждого числа, пропорционального последней цифре частного от деления числа на последнюю цифру:" << endl;
    work.veryHardAction();
    for (int element : work.getValues()) 
        cout << element << " "; 
    cout << endl;
}

int main(void) {
    work.DataPush(4444);
    work.DataPush(123);
    work.DataPush(1);
    work.DataPush(12);
    work.DataPush(20);
    start();
}