#define CATCH_CONFIG_MAIN
#include "Work.cpp"
#include <iostream>
#include <catch.hpp>
using namespace std;    

Work work;

TEST_CASE("Add To Vector", "[add_to_vector]") { 
    REQUIRE(work.DataPush(100) == true);
    REQUIRE(work.getValues().at(0) == 100);
    REQUIRE_THROWS(work.getValues().at(1000));
}

TEST_CASE("Edit Vector", "[edit_vector]") {
    REQUIRE(work.DataEditByID(0, 1) == true); 
    REQUIRE(work.getValues().at(0) == 1);
}

TEST_CASE("DELETE FROM VECTOR", "[delete_from_vector]") {
    REQUIRE(work.DataRemoveByID(0) == true);
}

TEST_CASE("multiple_and_bigger", "[mul_and_big]") {
    REQUIRE(work.DataPush(4444) == true);
    REQUIRE(work.DataPush(123) == true);
    REQUIRE(work.DataPush(1) == true);
    REQUIRE(work.DataPush(12) == true);
    REQUIRE(work.multipleAndBigger() == -1); 
    REQUIRE(work.DataPush(20) == true);
    REQUIRE(work.multipleAndBigger() == 20); 
}

TEST_CASE("bigger_int", "[bigger_int]") {
    REQUIRE(work.biggerInt() == 4444); 
}

TEST_CASE("remove_rank_of_tens_and_units_coincides", "[remove_rank_of_tens_and_units_coincides]") {
    work.removeRankOfTensAndUnitsCoincides();
    REQUIRE(work.getValues().at(0) == 1);
    REQUIRE(work.getValues().at(1) == 12);
    REQUIRE(work.getValues().at(2) == 20);
}

TEST_CASE("very_hard_action", "[very_hard_action]") {
    work.veryHardAction();
    REQUIRE(work.getValues().at(0) == 1);
    REQUIRE(work.getValues().at(1) == 1);
    REQUIRE(work.getValues().at(2) == 12);
    REQUIRE(work.getValues().at(3) == 6);
    REQUIRE(work.getValues().at(4) == 20);
    REQUIRE(work.getValues().at(5) == 123);
}