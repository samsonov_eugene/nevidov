#include <vector>
#include <iostream>
#include <algorithm>
#include <math.h>
using namespace std;

class Work {
    private: 
        vector<int> values;
    public: 
        bool multipleOfTen(int value);
        int biggerInt();
        vector<int> IntToIntVector(int num);
        void removeRankOfTensAndUnitsCoincides();
        void veryHardAction();
        int multipleAndBigger();
        bool DataPush(const int value);
        bool DataRemoveByID(const int id);
        bool DataEditByID(const int id, const int newValue);
        vector<int> getValues(); 
};