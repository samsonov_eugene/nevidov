#include "Work.h"

bool Work::multipleOfTen(int value) {
    if ((value%10) == 0) {
        return true;
    } else {
        return false;
    }
}
int Work::biggerInt() {
    sort(values.begin(), values.end()); 
    return values.at(values.size() - 1);
}

int Work::multipleAndBigger() {
    vector <int> multiplied;
    for (int element : values) {
        if (multipleOfTen(element)) {
            multiplied.push_back(element);
        }
    }
    sort(multiplied.begin(), multiplied.end());
    if (multiplied.size() > 0)
        return multiplied.at(multiplied.size() - 1);
    else 
        return -1;
}

vector<int> Work::IntToIntVector(int num) {
    vector<int> vec; 
    int counter = 0;
    while (num != 0) {
        vec.push_back(num % 10); 
        num /= 10; 
    }
    reverse(begin(vec), end(vec));
    return vec;
}
void Work::removeRankOfTensAndUnitsCoincides() {
    for (int i = 0; i < values.size(); i++) {
        vector<int> TMPVal = IntToIntVector(values.at(i)); 
        if (TMPVal.size() > 1) {
            if (TMPVal.at(TMPVal.size() - 1) == TMPVal.at(TMPVal.size() - 2)) {
                values.erase(values.begin() + i);
            }
        }
    }
}
void Work::veryHardAction() {
    vector<int> changed; 
    for (int i = 0; i < values.size(); i++) {
        int currentValue = values.at(i);
        changed.push_back(currentValue);
        vector <int> TMPVal = IntToIntVector(values.at(i));
        if (TMPVal.at(TMPVal.size() - 1 != 0)) {
            currentValue /= TMPVal.at(TMPVal.size() - 1);
            changed.push_back(currentValue);
        }
    }
    values = changed;
}
bool Work::DataPush(const int value) {
    try {
        values.push_back(value);
    } catch(...) {
        return false;
    }
    return true;
}
bool Work::DataRemoveByID(const int id) {
    try {
        values.erase(values.begin() + id); 
    } catch(...) {
        return false;
    }
    return true;
}
bool Work::DataEditByID(const int id, const int newValue) {
    try {
        values.at(id) = newValue;
    } catch(...) {
        return false;
    }
    return true;
}


vector<int> Work::getValues() {
    return values;
}