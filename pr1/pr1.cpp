#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h> 
#include <iostream>
using namespace std;

unsigned long long fib(long long n){
    if(n==1) {
        return 0;
    }
    if(n==2) {
    	return 1;
    }
    unsigned long long a1=0, a2=1, temp;
    for(long long i=1;i<n;i++) {
        temp=a2;
        a2=a1+a2;
        a1=temp;
    } 
    return a2;
}


long long sqr(long long n) {
	if (n == 0) return 1; 
	return 4 * sqr(n - 1);
}

long long factorial(long long n) {
	return (n < 1)? 1 : factorial (n - 1) * n; 
}
 
int main () { 
	long long TMPSqr = 2; 
	int eps = 150;
	double test =2;
    long long result = 0;
	clock_t begin = clock();
	for (long long i = 2; i < 30000; i++) {
        long long iteration_result = ((fib(i) * sqr(TMPSqr) * sqrt(i + 1)) / (factorial(i)));
        result += iteration_result;
		TMPSqr = TMPSqr + 2; 
	}
    clock_t end = clock();
    cout << "time: " << ((end-begin) / CLOCKS_PER_SEC) << endl; 
    // printf("%ld\n", end - begin);
    cout << "result: " << result << endl; 
    // printf("%d\n", result);
    return 0;
}